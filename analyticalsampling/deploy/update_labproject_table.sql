-- Deploy analyticalsampling:update_labproject_table to pg

BEGIN;

ALTER TABLE analyticalsampling.sample 
  DROP  CONSTRAINT "FK_sample_querycode";

ALTER TABLE analyticalsampling.study 
  DROP  CONSTRAINT "FK_study_querycode";

ALTER TABLE analyticalsampling.sample 
 DROP COLUMN IF EXISTS querycode_id;

ALTER TABLE analyticalsampling.study 
 DROP COLUMN IF EXISTS querycode_id;

DROP TABLE analyticalsampling.querycode;

ALTER TABLE platform.list 
 ALTER COLUMN record_uuid SET DEFAULT public.uuid_generate_v4();

ALTER TABLE platform.listmember 
 ALTER COLUMN record_uuid SET DEFAULT public.uuid_generate_v4();

CREATE TABLE analyticalsampling.labproject
(
	name varchar(50) NULL,
	description varchar(200) NULL,
	code varchar(50) NULL,
	purpose_code varchar(200) NULL,
	last_sample_number integer NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."labproject_id_seq"'::text)::regclass),
	serviceprovider_id integer NULL
)
;

CREATE SEQUENCE analyticalsampling.labproject_id_seq INCREMENT 1 START 1;

ALTER TABLE analyticalsampling.labproject ADD CONSTRAINT "PK_labproject"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.labproject ADD CONSTRAINT "FK_labproject_serviceprovider"
	FOREIGN KEY (serviceprovider_id) REFERENCES analyticalsampling.serviceprovider (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.sample ADD CONSTRAINT "FK_sample_labproject"
	FOREIGN KEY (labproject_id) REFERENCES analyticalsampling.labproject (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.study ADD CONSTRAINT "FK_study_labproject"
	FOREIGN KEY (labproject_id) REFERENCES analyticalsampling.labproject (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON COLUMN germplasm.germplasm.designation
	IS 'Designation: Standard designation/name of the germplasm [GERM_DESIGNATION]'
;

COMMENT ON COLUMN germplasm.germplasm.parentage
	IS 'Parentage: Parentage string of the germplasm [GERM_PARENTAGE]'
;

COMMENT ON COLUMN platform.list.record_uuid
	IS 'Universally unique identifier (UUID) of the record'
;

COMMENT ON COLUMN platform.listmember.record_uuid
	IS 'Universally unique identifier (UUID) of the record'
;

COMMENT ON COLUMN tenant.entityreference.entity
	IS ''
;

COMMENT ON COLUMN tenant.entityreference.storefield
	IS ''
;

COMMENT ON COLUMN tenant.entityreference.textfield
	IS ''
;

COMMENT ON COLUMN tenant.entityreference.valuefield
	IS ''
;

COMMIT;
