-- Deploy analyticalsampling:Add_PlantEntryList_Table to pg

BEGIN;

ALTER TABLE analyticalsampling.batch RENAME COLUMN objetive TO objective;

CREATE TABLE analyticalsampling.plantentrylist
(
	list_code varchar(50) NULL,
	list_name varchar(50) NULL,
	list_entry integer NULL,
	experiement_name varchar(50) NULL,
	occurrence_code varchar(50) NULL,
	site_name varchar(50) NULL,
	field_name varchar(50) NULL,
	experiment_year integer NULL,
	experiment_season varchar(50) NULL,
    exp_entry_number integer NULL,
	plot_code varchar(50) NULL,
	germplasm_code varchar(150) NULL,
	pedigree varchar(500) NULL,
	seed_generation varchar(50) NULL,
	seed_code varchar(50) NULL,
	material_type varchar(50) NULL,
	x integer NULL,
	y integer NULL,
	plant_number integer NULL,
	sh_status varchar(50) NULL,
	fto_status varchar(50) NULL,
	gmo_status varchar(50) NULL,
	material_class varchar(50) NULL,
	germplasm_status varchar(50) NULL,
	germplasm_state_fixed varchar(50) NULL,
	tenant_id integer NOT NULL,
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NOT NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."plantentrylist_id_seq"'::text)::regclass)
);

CREATE SEQUENCE analyticalsampling.plantentrylist_id_seq INCREMENT 1 START 1;

ALTER TABLE analyticalsampling.plantentrylist ADD CONSTRAINT "PK_plantentrylist"
PRIMARY KEY (id);

COMMIT;
