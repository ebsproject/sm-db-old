-- Deploy analyticalsampling:add_plate_name_column to pg

BEGIN;

ALTER TABLE analyticalsampling.sampledetail 
ALTER COLUMN uuid SET DATA TYPE uuid USING (uuid_generate_v4()); 

ALTER TABLE analyticalsampling.sampledetail 
ADD COLUMN plate_name varchar(250) NULL;

UPDATE analyticalsampling.batchcode SET last_plate_number = 0;

COMMIT;
