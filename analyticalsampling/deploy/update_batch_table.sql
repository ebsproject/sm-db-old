-- Deploy analyticalsampling:update_batch_table to pg

BEGIN;

ALTER TABLE analyticalsampling.containerset 
  DROP  CONSTRAINT "FK_containerset_study"
;
ALTER TABLE analyticalsampling.labproject 
  DROP  CONSTRAINT "FK_labproject_serviceprovider"
;
ALTER TABLE analyticalsampling.requestlistmember_study 
  DROP  CONSTRAINT "FK_requestlistmember_study_requestlistmember"
;
ALTER TABLE analyticalsampling.requestlistmember_study 
  DROP  CONSTRAINT "FK_requestlistmember_study_study"
;
ALTER TABLE analyticalsampling.sample 
  DROP  CONSTRAINT "FK_sample_labproject"
;
ALTER TABLE analyticalsampling.sampledetail 
  DROP  CONSTRAINT "FK_sampledetail_study"
;
ALTER TABLE analyticalsampling.study 
  DROP  CONSTRAINT "FK_study_collectionlayout"
;
ALTER TABLE analyticalsampling.study 
  DROP  CONSTRAINT "FK_study_crop"
;
ALTER TABLE analyticalsampling.study 
  DROP  CONSTRAINT "FK_study_labproject"
;
ALTER TABLE analyticalsampling.study 
  DROP  CONSTRAINT "FK_study_loadtype"
;
ALTER TABLE analyticalsampling.study 
  DROP  CONSTRAINT "FK_study_location"
;
ALTER TABLE analyticalsampling.study 
  DROP  CONSTRAINT "FK_study_tissuetype"
;
ALTER TABLE analyticalsampling.study_marker 
  DROP  CONSTRAINT "FK_study_marker_marker"
;
ALTER TABLE analyticalsampling.study_marker 
  DROP  CONSTRAINT "FK_study_marker_study"
;
ALTER TABLE analyticalsampling.study_trait 
  DROP  CONSTRAINT "FK_study_trait_study"
;
ALTER TABLE analyticalsampling.study_trait 
  DROP  CONSTRAINT "FK_study_trait_trait"
;
ALTER TABLE analyticalsampling.labproject 
  DROP  CONSTRAINT "PK_labproject"
;
ALTER TABLE analyticalsampling.study 
  DROP  CONSTRAINT "PK_study"
;

ALTER TABLE analyticalsampling.containerset 
 DROP COLUMN IF EXISTS study_id
;
ALTER TABLE analyticalsampling.labproject 
 DROP COLUMN IF EXISTS description
;
ALTER TABLE analyticalsampling.labproject 
 DROP COLUMN IF EXISTS purpose_code
;
ALTER TABLE analyticalsampling.requestlistmember_study 
 DROP COLUMN IF EXISTS study_id
;
ALTER TABLE analyticalsampling.sample 
 DROP COLUMN IF EXISTS labproject_id
;
ALTER TABLE analyticalsampling.sampledetail 
 DROP COLUMN IF EXISTS study_id
;
ALTER TABLE analyticalsampling.study 
 DROP COLUMN IF EXISTS labproject_id
;
ALTER TABLE analyticalsampling.study_marker 
 DROP COLUMN IF EXISTS study_id
;
ALTER TABLE analyticalsampling.study_trait 
 DROP COLUMN IF EXISTS study_id
;
ALTER TABLE platform.list 
 ALTER COLUMN record_uuid SET DEFAULT public.uuid_generate_v4()
;
ALTER TABLE platform.listmember 
 ALTER COLUMN record_uuid SET DEFAULT public.uuid_generate_v4()
;

ALTER TABLE analyticalsampling.labproject RENAME TO batchcode
;
ALTER TABLE analyticalsampling.requestlistmember_study RENAME TO requestlistmember_batch
;
ALTER TABLE analyticalsampling.study RENAME TO batch
;
ALTER TABLE analyticalsampling.study_marker RENAME TO batch_marker
;
ALTER TABLE analyticalsampling.study_trait RENAME TO batch_trait
;

ALTER TABLE analyticalsampling.batch 
 ADD COLUMN batchcode_id integer NULL
;
ALTER TABLE analyticalsampling.batch_marker 
 ADD COLUMN batch_id integer NULL
;
ALTER TABLE analyticalsampling.batch_trait 
 ADD COLUMN batch_id integer NULL
;
ALTER TABLE analyticalsampling.batchcode 
 ADD COLUMN last_batch_number integer NULL
;
ALTER TABLE analyticalsampling.containerset 
 ADD COLUMN batch_id integer NOT NULL
;
ALTER TABLE analyticalsampling.requestlistmember_batch 
 ADD COLUMN batch_id integer NULL
;
ALTER TABLE analyticalsampling.sample 
 ADD COLUMN batchcode_id integer NULL
;
ALTER TABLE analyticalsampling.sampledetail 
 ADD COLUMN batch_id integer NOT NULL
;

ALTER TABLE analyticalsampling.batch ADD CONSTRAINT "PK_batch"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.batchcode ADD CONSTRAINT "PK_batchcode"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.batch ADD CONSTRAINT "FK_batch_batchcode"
	FOREIGN KEY (batchcode_id) REFERENCES analyticalsampling.batchcode (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.batch ADD CONSTRAINT "FK_batch_collectionlayout"
	FOREIGN KEY (collectionlayout_id) REFERENCES analyticalsampling.collectionlayout (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.batch ADD CONSTRAINT "FK_batch_crop"
	FOREIGN KEY (crop_id) REFERENCES tenant.crop (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.batch ADD CONSTRAINT "FK_batch_loadtype"
	FOREIGN KEY (loadtype_id) REFERENCES analyticalsampling.loadtype (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.batch ADD CONSTRAINT "FK_batch_location"
	FOREIGN KEY (location_id) REFERENCES experiment.location (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.batch ADD CONSTRAINT "FK_batch_tissuetype"
	FOREIGN KEY (tissuetype_id) REFERENCES analyticalsampling.tissuetype (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.batch_marker ADD CONSTRAINT "FK_batch_marker_batch"
	FOREIGN KEY (batch_id) REFERENCES analyticalsampling.batch (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.batch_marker ADD CONSTRAINT "FK_batch_marker_marker"
	FOREIGN KEY (marker_id) REFERENCES analyticalsampling.marker (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.batch_trait ADD CONSTRAINT "FK_batch_trait_batch"
	FOREIGN KEY (batch_id) REFERENCES analyticalsampling.batch (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.batch_trait ADD CONSTRAINT "FK_batch_trait_trait"
	FOREIGN KEY (trait_id) REFERENCES analyticalsampling.trait (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.batchcode ADD CONSTRAINT "FK_batchcode_serviceprovider"
	FOREIGN KEY (serviceprovider_id) REFERENCES analyticalsampling.serviceprovider (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.containerset ADD CONSTRAINT "FK_containerset_batch"
	FOREIGN KEY (batch_id) REFERENCES analyticalsampling.batch (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.requestlistmember_batch ADD CONSTRAINT "FK_requestlistmember_batch_batch"
	FOREIGN KEY (batch_id) REFERENCES analyticalsampling.batch (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.requestlistmember_batch ADD CONSTRAINT "FK_requestlistmember_batch_requestlistmember"
	FOREIGN KEY (requestlistmember_id) REFERENCES analyticalsampling.requestlistmember (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.sample ADD CONSTRAINT "FK_sample_batchcode"
	FOREIGN KEY (batchcode_id) REFERENCES analyticalsampling.batchcode (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.sampledetail ADD CONSTRAINT "FK_sampledetail_batch"
	FOREIGN KEY (batch_id) REFERENCES analyticalsampling.batch (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON COLUMN analyticalsampling.batch.batchcode_id
	IS ''
;

COMMENT ON COLUMN analyticalsampling.batch_marker.batch_id
	IS ''
;

COMMENT ON COLUMN analyticalsampling.batch_trait.batch_id
	IS ''
;

COMMENT ON COLUMN analyticalsampling.batchcode.last_batch_number
	IS ''
;

COMMENT ON COLUMN analyticalsampling.containerset.batch_id
	IS ''
;

COMMENT ON COLUMN analyticalsampling.purpose.description
	IS 'description of the purpose'
;

COMMENT ON COLUMN analyticalsampling.requestlistmember_batch.batch_id
	IS ''
;

COMMENT ON COLUMN analyticalsampling.sample.batchcode_id
	IS ''
;

COMMENT ON COLUMN analyticalsampling.sampledetail.batch_id
	IS ''
;

COMMENT ON COLUMN analyticalsampling.service.description
	IS 'Description of the Service'
;

COMMENT ON COLUMN germplasm.germplasm.designation
	IS 'Designation: Standard designation/name of the germplasm [GERM_DESIGNATION]'
;

COMMENT ON COLUMN germplasm.germplasm.parentage
	IS 'Parentage: Parentage string of the germplasm [GERM_PARENTAGE]'
;

COMMENT ON COLUMN platform.list.record_uuid
	IS 'Universally unique identifier (UUID) of the record'
;

COMMENT ON COLUMN platform.listmember.record_uuid
	IS 'Universally unique identifier (UUID) of the record'
;

COMMENT ON COLUMN tenant.action.action
	IS ''
;

COMMENT ON COLUMN tenant.action.description
	IS ''
;

COMMENT ON COLUMN tenant.entityreference.entity
	IS ''
;

COMMENT ON COLUMN tenant.entityreference.storefield
	IS ''
;

COMMENT ON COLUMN tenant.entityreference.textfield
	IS ''
;

COMMENT ON COLUMN tenant.entityreference.valuefield
	IS ''
;

COMMENT ON COLUMN workflow.workflow.definition
	IS ''
;

COMMIT;
