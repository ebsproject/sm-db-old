-- Deploy tenant:add_sm_tables to pg

BEGIN;


	CREATE SCHEMA experiment;
    CREATE SCHEMA platform;
    CREATE SCHEMA dictionary;
    CREATE SCHEMA germplasm;
   

CREATE OR REPLACE FUNCTION public.uuid_generate_v4()
 RETURNS uuid
 LANGUAGE c
 PARALLEL SAFE STRICT
AS '$libdir/uuid-ossp', $function$uuid_generate_v4$function$
;

CREATE TABLE analyticalsampling.collectioncontainertype
(
	name varchar(50) NULL,
	description varchar(50) NULL,
	code varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."collectioncontainertype_id_seq"'::text)::regclass)
)
;

CREATE TABLE analyticalsampling.collectionlayout
(
	num_columns integer NULL,
	num_rows integer NULL,
	num_well integer NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."collectionlayout_id_seq"'::text)::regclass),
	collectioncontainertype_id integer NULL
)
;

CREATE TABLE analyticalsampling.containerset
(
	name varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."containerset_id_seq"'::text)::regclass),
	storagelocation_id integer NULL,
	labcontrol_id integer NULL,
	study_id integer NOT NULL
)
;

CREATE TABLE analyticalsampling.controltype
(
	name varchar(50) NULL,
	code varchar(50) NULL,
	description varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."controltype_id_seq"'::text)::regclass)
)
;

CREATE TABLE analyticalsampling.historicalstorage
(
	name varchar(50) NULL,
	move_date varchar(50) NULL,
	move_type varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."historicalstorage_id_seq"'::text)::regclass),
	storagelocation_id integer NULL
)
;

CREATE TABLE analyticalsampling.labcontrol
(
	name varchar(50) NULL,
	description varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."labcontrol_id_seq"'::text)::regclass)
)
;

CREATE TABLE analyticalsampling.lablayout
(
	name varchar(50) NULL,
	description varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."lablayout_id_seq"'::text)::regclass),
	collectionlayout_id integer NULL,
	serviceprovider_id integer NULL
)
;

CREATE TABLE analyticalsampling.lablayoutcontrol
(
	"column" integer NULL,
	row varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."lablayoutcontrol_id_seq"'::text)::regclass),
	controltype_id integer NOT NULL,
	lablayout_id integer NOT NULL
)
;

CREATE TABLE analyticalsampling.loadtype
(
	name varchar(50) NULL,
	description varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."loadtype_id_seq"'::text)::regclass)
)
;

CREATE TABLE analyticalsampling.mixturemethod
(
	name varchar(50) NULL,
	code varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."mixturemethod_id_seq"'::text)::regclass)
)
;

CREATE TABLE analyticalsampling.querycode
(
	name varchar(50) NULL,
	description varchar(50) NULL,
	code varchar(50) NULL,
	purpose_code varchar(50) NULL,
	last_sample_number integer NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."querycode_id_seq"'::text)::regclass),
	serviceprovider_id integer NULL
)
;

CREATE TABLE analyticalsampling.requestlistmember
(
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."requestlistmember_id_seq"'::text)::regclass),
	listmember_id integer NULL,
	request_id integer NULL
)
;

CREATE TABLE analyticalsampling.requestlistmember_study
(
	study_id integer NULL,
	requestlistmember_id integer NULL
)
;

CREATE TABLE analyticalsampling.resultpreference
(
	name varchar(50) NULL,
	column_count integer NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."resultpreference_id_seq"'::text)::regclass),
	serviceprovider_id integer NULL
)
;

CREATE TABLE analyticalsampling.resultpreferencedetail
(
	fijo boolean NULL,
	paramid integer NULL,
	param_name varchar(50) NULL,
	header varchar(50) NULL,
	orden integer NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."resultpreferencedetail_id_seq"'::text)::regclass),
	resultpreference_id integer NOT NULL
)
;

CREATE TABLE analyticalsampling.sample
(
	sample_number integer NULL,
	data_id integer NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."sample_id_seq"'::text)::regclass),
	labproject_id integer NULL,
	location_id integer NULL,
	crop_id integer NULL,
	entity_id integer NULL,
	season_id integer NULL,
	querycode_id integer NULL,
	session_id integer NULL
)
;

CREATE TABLE analyticalsampling.sampledetail
(
	sample_number integer NULL,
	row varchar(50) NULL,
	uuid integer NULL,
	"column" integer NULL,
	data_id integer NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	containerset_id integer NOT NULL,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."sampledetail_id_seq"'::text)::regclass),
	controltype_id integer NULL,
	study_id integer NOT NULL,
	crop_id integer NULL,
	location_id integer NULL,
	listmember_id integer NULL,
	entity_id integer NULL,
	season_id integer NULL,
	session_id integer NULL
)
;

CREATE TABLE analyticalsampling.samplemixture
(
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."samplemixture_id_seq"'::text)::regclass)
)
;

CREATE TABLE analyticalsampling.samplemixturedetail
(
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."samplemixturedetail_id_seq"'::text)::regclass),
	mixturemethod_id integer NULL,
	sampledetail_id integer NULL,
	listmember_id integer NULL,
	samplemixture_id integer NOT NULL
)
;

CREATE TABLE analyticalsampling.shipment
(
	tracking_number_local varchar(50) NULL,
	tracking_number_delivery varchar(50) NULL,
	comment varchar(50) NULL,	-- We have to register the date register, date send and  date receive
	url_results varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."shipment_id_seq"'::text)::regclass),
	shipmentset_id integer NOT NULL,
	workflowinstance_id integer NULL
)
;

CREATE TABLE analyticalsampling.shipmentdetail
(
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."shipmentdetail_id_seq"'::text)::regclass),
	sampledetail_id integer NULL,
	shipmentset_id integer NOT NULL
)
;

CREATE TABLE analyticalsampling.shipmentset
(
	title varchar(50) NULL,
	description varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."shipmentset_id_seq"'::text)::regclass),
	serviceprovider_id integer NOT NULL
)
;

CREATE TABLE analyticalsampling.storagelocation
(
	name varchar(50) NULL,
	remarks varchar(50) NULL,
	code varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."storagelocation_id_seq"'::text)::regclass),
	storagelocation_id integer NULL
)
;

CREATE TABLE analyticalsampling.study
(
	name varchar(50) NULL,
	objetive varchar(50) NULL,
	start_date varchar(50) NULL,
	end_date varchar(50) NULL,
	num_containers integer NULL,
	num_members integer NULL,
	num_controls integer NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."study_id_seq"'::text)::regclass),
	collectionlayout_id integer NULL,
	labproject_id integer NULL,
	loadtype_id integer NULL,
	tissuetype_id integer NULL,
	location_id integer NULL,
	crop_id integer NULL,
	querycode_id integer NULL
)
;

CREATE TABLE analyticalsampling.study_marker
(
	marker_id integer NULL,
	study_id integer NULL
)
;

CREATE TABLE analyticalsampling.study_trait
(
	trait_id integer NULL,
	study_id integer NULL
)
;

CREATE TABLE analyticalsampling.tissuetype
(
	name varchar(50) NULL,
	description varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."tissuetype_id_seq"'::text)::regclass)
)
;

CREATE TABLE analyticalsampling.vendor_shipment
(
	shipment_id integer NULL,
	vendor_id integer NULL
)
;

CREATE TABLE dictionary.entity
(
	abbrev varchar(50) NULL,
	name varchar(50) NULL,
	description varchar(50) NULL,
	remarks varchar(50) NULL,
	notes varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('dictionary."entity_id_seq"'::text)::regclass)
)
;

CREATE TABLE experiment.location
(
	location_code varchar(50) NULL,	-- Location Code: Textual identifier of the location [LOC_CODE]
	location_name varchar(50) NULL,	-- Location Name: Full name of the location [LOC_NAME]
	location_status varchar(50) NULL,	-- Location Status: Status of the location {draft, operational, completed} [LOC_STATUS]
	location_type varchar(50) NULL,	-- Location Type: Type of the location, which can be fixed or movable {greenhouse, base (zone in a greenhouse with pots/plants), etc.} [LOC_TYPE]
	description varchar(50) NULL,	-- Location Description: Additional information about the location [LOC_DESC]
	location_planting_date varchar(50) NULL,	-- Location Planting Date: Date in general when the entries in the location were planted [LOC_PLANTDATE]
	location_harvest_date varchar(50) NULL,	-- Location Harvest Date: Date in general when the plots in the location were harvested [LOC_HARVDATE]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	location_document varchar(50) NULL,	-- Sorted list of distinct lexemes which are normalized; used in search query
	location_year integer NULL,	-- Location Year: Experiment location year
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('experiment."location_id_seq"'::text)::regclass)
)
;

CREATE TABLE experiment.plant
(
	plant_code varchar(50) NULL,	-- Plant Code: Textual identifier of the plant [PLANT_CODE]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('experiment."plant_id_seq"'::text)::regclass),
	plot_id integer NULL,
	subplot_id integer NULL
)
;

CREATE TABLE experiment.plot
(
	plot_code varchar(50) NULL,	-- Plot Code: Textual identifier of the plot within the location, which is used in data analysis [PLOT_CODE]
	plot_number integer NULL,	-- Plot Number: Ordering of the plot within the location, which is used in data analysis [PLOT_NO]
	plot_type varchar(50) NULL,	-- Plot Type: Type of the plot {options depend on the organization} [PLOT_TYPE]
	rep integer NULL,	-- Plot Replication: Replication number of the entry in the plot [PLOT_REP]
	design_x integer NULL,	-- Design X: Coordinate paired with Design Y that is generated by randomization and is used for data analysis [PLOT_DESX]
	design_y integer NULL,	-- Design Y: Coordinate paired with Design X that is generated by randomization and is used for data analysis [PLOT_DESY]
	plot_order_number integer NULL,	-- Plot Order Number: Order number of plot in the planting area [PLOT_ORDERNO]
	pa_x integer NULL,	-- Planting Area X: Coordinate paired with Planting Area Y that is generated by KDX or other tools and is used for mapping the plots in the planting area [PLOT_PAX]
	pa_y integer NULL,	-- Planting Area Y: Coordinate paired with Planting Area X that is generated by KDX or other tools and is used for mapping the plots in the planting area [PLOT_PAY]
	field_x integer NULL,	-- Field X: Coordinate paired with Field Y that determines the placement of the plot in the field [PLOT_FLDX]
	field_y integer NULL,	-- Field Y: Coordinate paired with Field X that determines the placement of the plot in the field [PLOT_FLDY]
	plot_status varchar(50) NULL,	-- Plot Status: Status of the plot {draft, active, replaced} [PLOT_STATUS]
	plot_qc_code varchar(50) NULL,	-- Plot QC Code: Quality control code assigned to the plot {G (good), Q (questionable), S (suppressed)} [PLOT_QCCODE]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('experiment."plot_id_seq"'::text)::regclass),
	location_id integer NULL
)
;

CREATE TABLE experiment.subplot
(
	subplot_code varchar(50) NULL,	-- Subplot Code: Textual identifier of the subplot [SUBPLOT_CODE]
	subplot_number integer NULL,	-- Subplot Number: Sequential number of the subplot within the plot [SUBPLOT_NO]
	subplot_type varchar(50) NULL,	-- Subplot Type: Type of the subplot {hill, row, any part of plant, ...} [SUBPLOT_TYPE]
	subplot_z_number integer NULL,	-- Subplot Z Number: Ordering of subplot in the plot or another subplot [SUBPLOT_ZNO]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('experiment."subplot_id_seq"'::text)::regclass),
	plot_id integer NULL,
	subplot_id integer NULL
)
;

CREATE TABLE germplasm."cross"
(
	cross_name varchar(50) NULL,	-- Cross Name: Name of the cross, temporarily set to the combination of the parents' names (A/B) but when marked as successfull, it is assigned with a cross name that complies with the naming convention of the organization [CROSS_NAME]
	cross_method varchar(50) NULL,	-- Cross Method: Method of crossing used in the cross {single cross, backcross, ...} [CROSS_METHOD]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."cross_id_seq"'::text)::regclass),
	germplasm_id integer NULL,
	seed_id integer NULL
)
;

CREATE TABLE germplasm.cross_attribute
(
	data_value varchar(50) NULL,	-- Cross Attribute Data Value: Value of the cross attribute [CROSSATTR_DATAVAL]
	data_qc_code varchar(50) NULL,	-- Cross Attribute Data QC Code: Quality of the cross attribute {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [CROSSATTR_QCCODE]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."cross_attribute_id_seq"'::text)::regclass),
	cross_id integer NULL
)
;

CREATE TABLE germplasm.cross_parent
(
	parent_role varchar(50) NULL,	-- Parent Role: Role of the parent in the cross {female, male} [CPARENT_PARENTROLE]
	order_number integer NULL,	-- Order Number: Ordering of parents in the cross [CPARENT_ORDERNO]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."cross_parent_id_seq"'::text)::regclass),
	cross_id integer NULL,
	germplasm_id integer NULL,
	plot_id integer NULL,
	seed_id integer NULL
)
;

CREATE TABLE germplasm.envelope
(
	envelope_code integer NULL,	-- Envelope Code: Textual identifier of the envelope [ENVLP_CODE]
	envelope_label varchar(50) NULL,	-- Envelope Label: Human-readable text printed on the envelope [ENVLP_LABEL]
	order_number integer NULL,	-- Envelope Order Number: Ordering of the envelope within the package [ENVLP_ORDERNO]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."envelope_id_seq"'::text)::regclass),
	package_id integer NULL
)
;

CREATE TABLE germplasm.family
(
	selection_number integer NULL,	-- Selection Number: Sequence order of the germplasm from the family, if applicable [FAM_SELNO]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."family_id_seq"'::text)::regclass),
	cross_id integer NULL,
	parent_germplasm_id integer NULL,
	child_germplasm_id integer NULL,
	seed_id integer NULL
)
;

CREATE TABLE germplasm.genetic_population
(
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."genetic_population_id_seq"'::text)::regclass),
	germplasm_id integer NULL,
	seed_id integer NULL
)
;

CREATE TABLE germplasm.genetic_population_attribute
(
	variable_id integer NULL,	-- Reference to the variable of the genetic population attribute
	data_value varchar(50) NULL,	-- Value of the genetic population attribute
	data_qc_code varchar(50) NULL,	-- Status of the genetic population attribute  {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)}
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."genetic_population_attribute_id_seq"'::text)::regclass),
	genetic_population_id integer NULL
)
;

CREATE TABLE germplasm.germplasm
(
	designation varchar(256) NULL,	-- Designation: Standard designation/name of the germplasm [GERM_DESIGNATION]
	parentage varchar(600) NULL,	-- Parentage: Parentage string of the germplasm [GERM_PARENTAGE]
	generation varchar(50) NULL,	-- Generation: Stage of development of a germplasm [GERM_GENERATION]
	germplasm_state varchar(50) NULL,	-- Germplasm State: Current state of the germplasm: (progeny, fixed line, ...) [GERM_STATE]
	germplasm_name_type varchar(50) NULL,	-- Germplasm Name Type: Type of name designated to the germplasm [GERM_NAMETYPE]
	germplasm_normalized_name varchar(50) NULL,	-- Germplasm Normalized Name: Formatted germplasm name that is standardized for query purposes [GERM_NORMNAME]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."germplasm_id_seq"'::text)::regclass),
	crop_id integer NULL,
	product_profile_id integer NULL,
	taxonomy_id integer NULL
)
;

CREATE TABLE germplasm.germplasm_attribute
(
	data_value varchar(50) NULL,	-- Germplasm Attribute Data Value: Value of the germplasm attribute [GERMATTR_DATAVAL]
	data_qc_code varchar(50) NULL,	-- Germlasm Attribute Data QC Code: Status of the germplasm attribute {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [GERMATTR_QCCODE]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."germplasm_attribute_id_seq"'::text)::regclass),
	germplasm_id integer NULL
)
;

CREATE TABLE germplasm.germplasm_name
(
	name_value varchar(50) NULL,	-- Germplasm Name Value: Value of the name designated to the germplasm [GERMNAME_NAMEVAL]
	germplasm_name_type varchar(50) NULL,	-- Germplasm Name Type: Type of name designated to the germplasm [GERMNAME_NAMETYPE]
	germplasm_name_status varchar(50) NULL,	-- Germplasm Name Status: Status of the germplasm name {standard, active, deprecated, inactive, voided} [GERMNAME_NAMESTATUS]
	germplasm_normalized_name varchar(50) NULL,	-- Germplasm Normalized Name: Formatted germplasm name that is standardized for query purposes [GERMNAME_NORMNAME]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."germplasm_name_id_seq"'::text)::regclass),
	germplasm_id integer NULL
)
;

CREATE TABLE germplasm.germplasm_relation
(
	order_number integer NULL,	-- Germplasm Relation Order Number: Ordering of the child germplasm within the parent germplasm [GERMREL_ORDERNO]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."germplasm_relation_id_seq"'::text)::regclass),
	germplasm_id integer NULL
)
;

CREATE TABLE germplasm.germplasm_state_attribute
(
	germplasm_state_type varchar(50) NULL,	-- Germplasm State Type: Type of state of the germplasm [GSATTR_STATETYPE]
	germplasm_state_rationale varchar(50) NULL,	-- Germplasm State Rationale: Reason of the state of the germplasm [GSATTR_RATIONALE]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."germplasm_state_attribute_id_seq"'::text)::regclass),
	germplasm_id integer NULL
)
;

CREATE TABLE germplasm.germplasm_trait
(
	data_value varchar(50) NULL,	-- Germplasm Trait Data Value: Value of the germplasm trait [GERMTRAIT_DATAVAL]
	data_qc_code varchar(50) NULL,	-- Germplasm Trait Data QC Code: Quality of the germplasm trait {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [GERMTRAIT_QCCODE]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."germplasm_trait_id_seq"'::text)::regclass),
	germplasm_id integer NULL
)
;

CREATE TABLE germplasm.package
(
	package_code varchar(50) NULL,	-- Package Code: Textual identifier of the package [PKG]
	package_label varchar(50) NULL,	-- Package Label: Human-readable text printed on the seed package [PKG_LABEL]
	package_quantity integer NULL,	-- Package Quantity: Weight or number of seeds in the package [PKG_QTY]
	package_unit varchar(50) NULL,	-- Package Unit: Unit of quantity of the seed package [PKG_UNIT]
	package_status varchar(50) NULL,	-- Package Status: Status of the package {list values here} [PKG_STATUS]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."package_id_seq"'::text)::regclass),
	seed_id integer NULL
)
;

CREATE TABLE germplasm.package_relation
(
	relation_type varchar(50) NULL,	-- Relation Type: Type of relation {combined (one or more source packages to produce one destination package), divided (one source package to produce one or many destination packages), subset (one source package to product one or many destination packages without the need to generate new seeds)} [PKGREL_RELTYPE]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."package_relation_id_seq"'::text)::regclass),
	source_package_id integer NULL,
	destination_package_id integer NULL
)
;

CREATE TABLE germplasm.package_trait
(
	data_value varchar(50) NULL,	-- Package Trait Data Value: Value of the package trait [PKGTRAIT_DATAVAL]
	data_qc_code varchar(50) NULL,	-- Package Trait Data QC Code: Quality of the package trait {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [PKGTRAIT_QCCODE]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."package_trait_id_seq"'::text)::regclass),
	package_id integer NULL
)
;

CREATE TABLE germplasm.product_profile
(
	product_profile_code varchar(50) NULL,	-- Product Profile Code: Textual identifier of the product profile [PRODPROF_CODE]
	product_profile_name varchar(50) NULL,	-- Product Profile Name: Full name of the product profile [PRODPROF_NAME]
	description varchar(50) NULL,	-- Product Profile Description: Additional information about the product profile [PRODPROF_DESC]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."product_profile_id_seq"'::text)::regclass)
)
;

CREATE TABLE germplasm.product_profile_attribute
(
	variable_id integer NULL,	-- Variable ID: Referene to the variable of the product profile data [PPATTR_VAR_ID]
	data_value varchar(50) NULL,	-- Product Profile Attribute Value: Value of the product profile attribute [PPATTR_DATAVAL]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."product_profile_attribute_id_seq"'::text)::regclass),
	product_profile_id integer NULL
)
;

CREATE TABLE germplasm.seed
(
	seed_code varchar(50) NULL,	-- Seed Code: Textual identifier of the seed [SEED_CODE]
	seed_name varchar(50) NULL,	-- Seed Name: Full name of the germplasm at the time the seed was harvested [SEED_NAME]
	harvest_date varchar(50) NULL,	-- Seed Harvest Date: Date when the seeds were harvested from the location of an experiment [SEED_HARVESTDATE]
	harvest_method varchar(50) NULL,	-- Harvest Method: Method of harvesting the plots in the location [SEED_HARVMETH]
	description varchar(50) NULL,	-- Seed Description: Additional information about the seed [SEED_DESC]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."seed_id_seq"'::text)::regclass),
	location_id integer NULL,
	plot_id integer NULL,
	germplasm_id integer NULL
)
;

CREATE TABLE germplasm.seed_attribute
(
	data_value varchar(50) NULL,	-- Seed Attribute Data Value: Value of the seed attribute [SEEDATTR_DATAVAL]
	data_qc_code varchar(50) NULL,	-- Seed Attribute Data QC Code: Quality of the seed attribute {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [SEEDATTR_QCCODE]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."seed_attribute_id_seq"'::text)::regclass),
	seed_id integer NULL
)
;

CREATE TABLE germplasm.seed_relation
(
	order_number integer NULL,	-- Seed Relation Order Number: Order of the child seed within the parent seed [SEEDREL_ORDERNO]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."seed_relation_id_seq"'::text)::regclass),
	parent_seed_id integer NULL,
	child_seed_id integer NULL
)
;

CREATE TABLE germplasm.seed_trait
(
	data_value integer NULL,	-- Seed Trait Data Value: Value of the seed trait [SEEDTRAIT_DATAVAL]
	data_qc_code varchar(50) NULL,	-- Seed Trait Data QC Code: Status of the seed trait {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [SEEDTRAIT_QCCODE]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."seed_trait_id_seq"'::text)::regclass),
	seed_id integer NULL
)
;

CREATE TABLE germplasm.taxonomy
(
	taxon_id varchar(50) NULL,	-- Taxon ID: Recognized identity or essential character of the germplasm [TAXON_ID]
	taxonomy_name varchar(50) NULL,	-- Taxonomy Name: Full name of the taxonomic identity [TAXON_NAME]
	description varchar(50) NULL,	-- Taxonomy Description: Additional information about the taxonomy [TAXON_DESC]
	notes varchar(50) NULL,	-- NOTES: Technical details about the record [ISVOID]
	event_log varchar(50) NULL,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('germplasm."taxonomy_id_seq"'::text)::regclass)
)
;

CREATE TABLE platform.list
(
	abbrev varchar(50) NULL,	-- Abbrev of the list. must be unique
	name varchar(50) NULL,	-- Name of the list
	display_name varchar(50) NULL,	-- Display name of the list
	type varchar(50) NULL,	-- List type can be designation, plot, seed, study, or location
	description varchar(50) NULL,	-- User description of the list
	remarks varchar(50) NULL,	-- Additional details about the record
	notes varchar(50) NULL,	-- Additional details added by an admin; can be technical or advanced details
	event_log varchar(50) NULL,	-- Historical transactions of the record
	record_uuid varchar(50) NULL   DEFAULT public.uuid_generate_v4(),	-- Universally unique identifier (UUID) of the record
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('platform."list_id_seq"'::text)::regclass),
	entity_id integer NULL
)
;

CREATE TABLE platform.listmember
(
	data_id integer NULL,	-- Data ID of the entity. If list type is study, data_id will refer to operational.study.id
	order_number integer NULL,	-- Order number of the member in the list
	display_value varchar(50) NULL,	-- Display value of the item member in list. Can be designation, study_name, or plot code
	remarks varchar(50) NULL,	-- Additional details about the record
	notes varchar(50) NULL,	-- Additional details added by an admin; can be technical or advanced details
	event_log varchar(50) NULL,	-- Historical transactions of the record
	record_uuid varchar(50) NULL   DEFAULT public.uuid_generate_v4(),	-- Universally unique identifier (UUID) of the record
	search_input varchar(50) NULL,
	is_active boolean NULL   DEFAULT true,
	tenant_id integer NULL,
	creation_timestamp timestamp without time zone NULL,
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('platform."listmember_id_seq"'::text)::regclass),
	list_id integer NULL
)
;

CREATE SEQUENCE analyticalsampling.collectioncontainertype_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.collectionlayout_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.containerset_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.controltype_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.historicalstorage_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.labcontrol_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.lablayout_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.lablayoutcontrol_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.loadtype_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.mixturemethod_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.querycode_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.requestlistmember_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.resultpreference_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.resultpreferencedetail_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.sample_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.sampledetail_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.samplemixture_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.samplemixturedetail_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.shipment_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.shipmentdetail_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.shipmentset_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.storagelocation_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.study_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE analyticalsampling.tissuetype_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE dictionary.entity_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE experiment.location_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE experiment.plant_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE experiment.plot_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE experiment.subplot_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.cross_attribute_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.cross_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.cross_parent_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.envelope_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.family_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.genetic_population_attribute_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.genetic_population_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.germplasm_attribute_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.germplasm_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.germplasm_name_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.germplasm_relation_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.germplasm_state_attribute_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.germplasm_trait_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.package_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.package_relation_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.package_trait_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.product_profile_attribute_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.product_profile_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.seed_attribute_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.seed_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.seed_relation_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.seed_trait_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE germplasm.taxonomy_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE platform.list_id_seq INCREMENT 1 START 1
;
CREATE SEQUENCE platform.listmember_id_seq INCREMENT 1 START 1
;

ALTER TABLE analyticalsampling.collectioncontainertype ADD CONSTRAINT "PK_collectioncontainertype"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.collectionlayout ADD CONSTRAINT "PK_collectionlayout"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.containerset ADD CONSTRAINT "PK_containerset"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.controltype ADD CONSTRAINT "PK_controltype"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.historicalstorage ADD CONSTRAINT "PK_historicalstorage"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.labcontrol ADD CONSTRAINT "PK_labcontrol"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.lablayout ADD CONSTRAINT "PK_lablayout"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.lablayoutcontrol ADD CONSTRAINT "PK_lablayoutcontrol"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.loadtype ADD CONSTRAINT "PK_loadtype"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.mixturemethod ADD CONSTRAINT "PK_mixturemethod"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.querycode ADD CONSTRAINT "PK_querycode"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.requestlistmember ADD CONSTRAINT "PK_requestlistmember"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.resultpreference ADD CONSTRAINT "PK_resultpreference"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.resultpreferencedetail ADD CONSTRAINT "PK_resultpreferencedetail"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.sample ADD CONSTRAINT "PK_sample"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.sampledetail ADD CONSTRAINT "PK_sampledetail"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.samplemixture ADD CONSTRAINT "PK_samplemixture"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.samplemixturedetail ADD CONSTRAINT "PK_samplemixturedetail"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.shipment ADD CONSTRAINT "PK_shipment"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.shipmentdetail ADD CONSTRAINT "PK_shipmentdetail"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.shipmentset ADD CONSTRAINT "PK_shipmentset"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.storagelocation ADD CONSTRAINT "PK_storagelocation"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.study ADD CONSTRAINT "PK_study"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.tissuetype ADD CONSTRAINT "PK_tissuetype"
	PRIMARY KEY (id)
;

ALTER TABLE dictionary.entity ADD CONSTRAINT "PK_entity"
	PRIMARY KEY (id)
;

ALTER TABLE experiment.location ADD CONSTRAINT "PK_location"
	PRIMARY KEY (id)
;

ALTER TABLE experiment.plant ADD CONSTRAINT "PK_plant"
	PRIMARY KEY (id)
;

ALTER TABLE experiment.plot ADD CONSTRAINT "PK_plot"
	PRIMARY KEY (id)
;

ALTER TABLE experiment.subplot ADD CONSTRAINT "PK_subplot"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm."cross" ADD CONSTRAINT "PK_cross"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.cross_attribute ADD CONSTRAINT "PK_cross_attribute"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.cross_parent ADD CONSTRAINT "PK_cross_parent"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.envelope ADD CONSTRAINT "PK_envelope"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.family ADD CONSTRAINT "PK_family"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.genetic_population ADD CONSTRAINT "PK_genetic_population"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.genetic_population_attribute ADD CONSTRAINT "PK_genetic_population_attribute"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.germplasm ADD CONSTRAINT "PK_germplasm"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.germplasm_attribute ADD CONSTRAINT "PK_germplasm_attribute"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.germplasm_name ADD CONSTRAINT "PK_germplasm_name"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.germplasm_relation ADD CONSTRAINT "PK_germplasm_relation"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.germplasm_state_attribute ADD CONSTRAINT "PK_germplasm_state_attribute"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.germplasm_trait ADD CONSTRAINT "PK_germplasm_trait"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.package ADD CONSTRAINT "PK_package"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.package_relation ADD CONSTRAINT "PK_package_relation"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.package_trait ADD CONSTRAINT "PK_package_trait"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.product_profile ADD CONSTRAINT "PK_product_profile"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.product_profile_attribute ADD CONSTRAINT "PK_product_profile_attribute"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.seed ADD CONSTRAINT "PK_seed"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.seed_attribute ADD CONSTRAINT "PK_seed_attribute"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.seed_relation ADD CONSTRAINT "PK_seed_relation"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.seed_trait ADD CONSTRAINT "PK_seed_trait"
	PRIMARY KEY (id)
;

ALTER TABLE germplasm.taxonomy ADD CONSTRAINT "PK_taxonomy"
	PRIMARY KEY (id)
;

ALTER TABLE platform.list ADD CONSTRAINT "PK_list"
	PRIMARY KEY (id)
;

ALTER TABLE platform.listmember ADD CONSTRAINT "PK_listmember"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.collectionlayout ADD CONSTRAINT "FK_collectionlayout_collectioncontainertype"
	FOREIGN KEY (collectioncontainertype_id) REFERENCES analyticalsampling.collectioncontainertype (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.containerset ADD CONSTRAINT "FK_containerset_labcontrol"
	FOREIGN KEY (labcontrol_id) REFERENCES analyticalsampling.labcontrol (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.containerset ADD CONSTRAINT "FK_containerset_storagelocation"
	FOREIGN KEY (storagelocation_id) REFERENCES analyticalsampling.storagelocation (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.containerset ADD CONSTRAINT "FK_containerset_study"
	FOREIGN KEY (study_id) REFERENCES analyticalsampling.study (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.historicalstorage ADD CONSTRAINT "FK_historicalstorage_storagelocation"
	FOREIGN KEY (storagelocation_id) REFERENCES analyticalsampling.storagelocation (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.lablayout ADD CONSTRAINT "FK_lablayout_collectionlayout"
	FOREIGN KEY (collectionlayout_id) REFERENCES analyticalsampling.collectionlayout (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.lablayout ADD CONSTRAINT "FK_lablayout_serviceprovider"
	FOREIGN KEY (serviceprovider_id) REFERENCES analyticalsampling.serviceprovider (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.lablayoutcontrol ADD CONSTRAINT "FK_lablayoutcontrol_controltype"
	FOREIGN KEY (controltype_id) REFERENCES analyticalsampling.controltype (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.lablayoutcontrol ADD CONSTRAINT "FK_lablayoutcontrol_lablayout"
	FOREIGN KEY (lablayout_id) REFERENCES analyticalsampling.lablayout (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.querycode ADD CONSTRAINT "FK_querycode_serviceprovider"
	FOREIGN KEY (serviceprovider_id) REFERENCES analyticalsampling.serviceprovider (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.requestlistmember ADD CONSTRAINT "FK_requestlistmember_listmember"
	FOREIGN KEY (listmember_id) REFERENCES platform.listmember (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.requestlistmember ADD CONSTRAINT "FK_requestlistmember_request"
	FOREIGN KEY (request_id) REFERENCES workflow.request (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.requestlistmember_study ADD CONSTRAINT "FK_requestlistmember_study_requestlistmember"
	FOREIGN KEY (requestlistmember_id) REFERENCES analyticalsampling.requestlistmember (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.requestlistmember_study ADD CONSTRAINT "FK_requestlistmember_study_study"
	FOREIGN KEY (study_id) REFERENCES analyticalsampling.study (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.resultpreference ADD CONSTRAINT "FK_resultpreference_serviceprovider"
	FOREIGN KEY (serviceprovider_id) REFERENCES analyticalsampling.serviceprovider (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.resultpreferencedetail ADD CONSTRAINT "FK_resultpreferencedetail_resultpreference"
	FOREIGN KEY (resultpreference_id) REFERENCES analyticalsampling.resultpreference (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.sample ADD CONSTRAINT "FK_sample_crop"
	FOREIGN KEY (crop_id) REFERENCES tenant.crop (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.sample ADD CONSTRAINT "FK_sample_entity"
	FOREIGN KEY (entity_id) REFERENCES dictionary.entity (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.sample ADD CONSTRAINT "FK_sample_location"
	FOREIGN KEY (location_id) REFERENCES experiment.location (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.sample ADD CONSTRAINT "FK_sample_querycode"
	FOREIGN KEY (querycode_id) REFERENCES analyticalsampling.querycode (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.sample ADD CONSTRAINT "FK_sample_season"
	FOREIGN KEY (season_id) REFERENCES tenant.season (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.sample ADD CONSTRAINT "FK_sample_session"
	FOREIGN KEY (session_id) REFERENCES tenant.session (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.sampledetail ADD CONSTRAINT "FK_sampledetail_containerset"
	FOREIGN KEY (containerset_id) REFERENCES analyticalsampling.containerset (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.sampledetail ADD CONSTRAINT "FK_sampledetail_controltype"
	FOREIGN KEY (controltype_id) REFERENCES analyticalsampling.controltype (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.sampledetail ADD CONSTRAINT "FK_sampledetail_crop"
	FOREIGN KEY (crop_id) REFERENCES tenant.crop (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.sampledetail ADD CONSTRAINT "FK_sampledetail_entity"
	FOREIGN KEY (entity_id) REFERENCES dictionary.entity (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.sampledetail ADD CONSTRAINT "FK_sampledetail_listmember"
	FOREIGN KEY (listmember_id) REFERENCES platform.listmember (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.sampledetail ADD CONSTRAINT "FK_sampledetail_location"
	FOREIGN KEY (location_id) REFERENCES experiment.location (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.sampledetail ADD CONSTRAINT "FK_sampledetail_season"
	FOREIGN KEY (season_id) REFERENCES tenant.season (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.sampledetail ADD CONSTRAINT "FK_sampledetail_session"
	FOREIGN KEY (session_id) REFERENCES tenant.session (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.sampledetail ADD CONSTRAINT "FK_sampledetail_study"
	FOREIGN KEY (study_id) REFERENCES analyticalsampling.study (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.samplemixturedetail ADD CONSTRAINT "FK_samplemixturedetail_listmember"
	FOREIGN KEY (listmember_id) REFERENCES platform.listmember (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.samplemixturedetail ADD CONSTRAINT "FK_samplemixturedetail_mixturemethod"
	FOREIGN KEY (mixturemethod_id) REFERENCES analyticalsampling.mixturemethod (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.samplemixturedetail ADD CONSTRAINT "FK_samplemixturedetail_sampledetail"
	FOREIGN KEY (sampledetail_id) REFERENCES analyticalsampling.sampledetail (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.samplemixturedetail ADD CONSTRAINT "FK_samplemixturedetail_samplemixture"
	FOREIGN KEY (samplemixture_id) REFERENCES analyticalsampling.samplemixture (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.shipment ADD CONSTRAINT "FK_shipment_shipmentset"
	FOREIGN KEY (shipmentset_id) REFERENCES analyticalsampling.shipmentset (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.shipment ADD CONSTRAINT "FK_shipment_workflowinstance"
	FOREIGN KEY (workflowinstance_id) REFERENCES workflow.workflowinstance (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.shipmentdetail ADD CONSTRAINT "FK_shipmentdetail_sampledetail"
	FOREIGN KEY (sampledetail_id) REFERENCES analyticalsampling.sampledetail (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.shipmentdetail ADD CONSTRAINT "FK_shipmentdetail_shipmentset"
	FOREIGN KEY (shipmentset_id) REFERENCES analyticalsampling.shipmentset (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.shipmentset ADD CONSTRAINT "FK_shipmentset_serviceprovider"
	FOREIGN KEY (serviceprovider_id) REFERENCES analyticalsampling.serviceprovider (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.storagelocation ADD CONSTRAINT "FK_storagelocation_storagelocation"
	FOREIGN KEY (storagelocation_id) REFERENCES analyticalsampling.storagelocation (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.study ADD CONSTRAINT "FK_study_collectionlayout"
	FOREIGN KEY (collectionlayout_id) REFERENCES analyticalsampling.collectionlayout (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.study ADD CONSTRAINT "FK_study_crop"
	FOREIGN KEY (crop_id) REFERENCES tenant.crop (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.study ADD CONSTRAINT "FK_study_loadtype"
	FOREIGN KEY (loadtype_id) REFERENCES analyticalsampling.loadtype (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.study ADD CONSTRAINT "FK_study_location"
	FOREIGN KEY (location_id) REFERENCES experiment.location (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.study ADD CONSTRAINT "FK_study_querycode"
	FOREIGN KEY (querycode_id) REFERENCES analyticalsampling.querycode (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.study ADD CONSTRAINT "FK_study_tissuetype"
	FOREIGN KEY (tissuetype_id) REFERENCES analyticalsampling.tissuetype (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.study_marker ADD CONSTRAINT "FK_study_marker_marker"
	FOREIGN KEY (marker_id) REFERENCES analyticalsampling.marker (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.study_marker ADD CONSTRAINT "FK_study_marker_study"
	FOREIGN KEY (study_id) REFERENCES analyticalsampling.study (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.study_trait ADD CONSTRAINT "FK_study_trait_study"
	FOREIGN KEY (study_id) REFERENCES analyticalsampling.study (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.study_trait ADD CONSTRAINT "FK_study_trait_trait"
	FOREIGN KEY (trait_id) REFERENCES analyticalsampling.trait (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.vendor_shipment ADD CONSTRAINT "FK_vendor_shipment_shipment"
	FOREIGN KEY (shipment_id) REFERENCES analyticalsampling.shipment (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.vendor_shipment ADD CONSTRAINT "FK_vendor_shipment_vendor"
	FOREIGN KEY (vendor_id) REFERENCES tenant.vendor (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE experiment.plant ADD CONSTRAINT "FK_plant_plot"
	FOREIGN KEY (plot_id) REFERENCES experiment.plot (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE experiment.plant ADD CONSTRAINT "FK_plant_subplot"
	FOREIGN KEY (subplot_id) REFERENCES experiment.subplot (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE experiment.plot ADD CONSTRAINT "FK_plot_location"
	FOREIGN KEY (location_id) REFERENCES experiment.location (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE experiment.subplot ADD CONSTRAINT "FK_subplot_plot"
	FOREIGN KEY (plot_id) REFERENCES experiment.plot (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE experiment.subplot ADD CONSTRAINT "FK_subplot_subplot"
	FOREIGN KEY (subplot_id) REFERENCES experiment.subplot (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm."cross" ADD CONSTRAINT "FK_cross_germplasm"
	FOREIGN KEY (germplasm_id) REFERENCES germplasm.germplasm (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm."cross" ADD CONSTRAINT "FK_cross_seed"
	FOREIGN KEY (seed_id) REFERENCES germplasm.seed (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.cross_attribute ADD CONSTRAINT "FK_cross_attribute_cross"
	FOREIGN KEY (cross_id) REFERENCES germplasm."cross" (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.cross_parent ADD CONSTRAINT "FK_cross_parent_cross"
	FOREIGN KEY (cross_id) REFERENCES germplasm."cross" (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.cross_parent ADD CONSTRAINT "FK_cross_parent_germplasm"
	FOREIGN KEY (germplasm_id) REFERENCES germplasm.germplasm (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.cross_parent ADD CONSTRAINT "FK_cross_parent_plot"
	FOREIGN KEY (plot_id) REFERENCES experiment.plot (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.cross_parent ADD CONSTRAINT "FK_cross_parent_seed"
	FOREIGN KEY (seed_id) REFERENCES germplasm.seed (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.envelope ADD CONSTRAINT "FK_envelope_package"
	FOREIGN KEY (package_id) REFERENCES germplasm.package (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.family ADD CONSTRAINT "FK_family_cross"
	FOREIGN KEY (cross_id) REFERENCES germplasm."cross" (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.family ADD CONSTRAINT "FK_family_germplasm"
	FOREIGN KEY (parent_germplasm_id) REFERENCES germplasm.germplasm (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.family ADD CONSTRAINT "FK_family_seed"
	FOREIGN KEY (seed_id) REFERENCES germplasm.seed (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.genetic_population ADD CONSTRAINT "FK_genetic_population_germplasm"
	FOREIGN KEY (germplasm_id) REFERENCES germplasm.germplasm (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.genetic_population ADD CONSTRAINT "FK_genetic_population_seed"
	FOREIGN KEY (seed_id) REFERENCES germplasm.seed (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.genetic_population_attribute ADD CONSTRAINT "FK_genetic_population_attribute_genetic_population"
	FOREIGN KEY (genetic_population_id) REFERENCES germplasm.genetic_population (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.germplasm ADD CONSTRAINT "FK_germplasm_crop"
	FOREIGN KEY (crop_id) REFERENCES tenant.crop (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.germplasm ADD CONSTRAINT "FK_germplasm_product_profile"
	FOREIGN KEY (product_profile_id) REFERENCES germplasm.product_profile (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.germplasm ADD CONSTRAINT "FK_germplasm_taxonomy"
	FOREIGN KEY (taxonomy_id) REFERENCES germplasm.taxonomy (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.germplasm_attribute ADD CONSTRAINT "FK_germplasm_attribute_germplasm"
	FOREIGN KEY (germplasm_id) REFERENCES germplasm.germplasm (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.germplasm_name ADD CONSTRAINT "FK_germplasm_name_germplasm"
	FOREIGN KEY (germplasm_id) REFERENCES germplasm.germplasm (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.germplasm_relation ADD CONSTRAINT "FK_germplasm_relation_germplasm"
	FOREIGN KEY (germplasm_id) REFERENCES germplasm.germplasm (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.germplasm_state_attribute ADD CONSTRAINT "FK_germplasm_state_attribute_germplasm"
	FOREIGN KEY (germplasm_id) REFERENCES germplasm.germplasm (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.germplasm_trait ADD CONSTRAINT "FK_germplasm_trait_germplasm"
	FOREIGN KEY (germplasm_id) REFERENCES germplasm.germplasm (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.package ADD CONSTRAINT "FK_package_seed"
	FOREIGN KEY (seed_id) REFERENCES germplasm.seed (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.package_relation ADD CONSTRAINT "FK_package_relation_package"
	FOREIGN KEY (source_package_id) REFERENCES germplasm.package (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.package_trait ADD CONSTRAINT "FK_package_trait_package"
	FOREIGN KEY (package_id) REFERENCES germplasm.package (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.product_profile_attribute ADD CONSTRAINT "FK_product_profile_attribute_product_profile"
	FOREIGN KEY (product_profile_id) REFERENCES germplasm.product_profile (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.seed ADD CONSTRAINT "FK_seed_germplasm"
	FOREIGN KEY (germplasm_id) REFERENCES germplasm.germplasm (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.seed ADD CONSTRAINT "FK_seed_location"
	FOREIGN KEY (location_id) REFERENCES experiment.location (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.seed ADD CONSTRAINT "FK_seed_plot"
	FOREIGN KEY (plot_id) REFERENCES experiment.plot (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.seed_attribute ADD CONSTRAINT "FK_seed_attribute_seed"
	FOREIGN KEY (seed_id) REFERENCES germplasm.seed (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.seed_relation ADD CONSTRAINT "FK_seed_relation_seed"
	FOREIGN KEY (parent_seed_id) REFERENCES germplasm.seed (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE germplasm.seed_trait ADD CONSTRAINT "FK_seed_trait_seed"
	FOREIGN KEY (seed_id) REFERENCES germplasm.seed (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE platform.list ADD CONSTRAINT "FK_list_entity"
	FOREIGN KEY (entity_id) REFERENCES dictionary.entity (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE platform.listmember ADD CONSTRAINT "FK_listmember_list"
	FOREIGN KEY (list_id) REFERENCES platform.list (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON TABLE analyticalsampling.collectionlayout
	IS 'The way in which the containers are arranged or laid out, in the case of tubes, it shows the size'
;

COMMENT ON TABLE analyticalsampling.containerset
	IS 'Set of containers'
;

COMMENT ON TABLE analyticalsampling.historicalstorage
	IS 'Shows the different locations where a seed was stored in certain point of time.'
;

COMMENT ON TABLE analyticalsampling.labcontrol
	IS 'Different type of controls conducted in the laboratory.'
;

COMMENT ON TABLE analyticalsampling.lablayout
	IS 'Layout control determined by the laboratory to manage their templates.'
;

COMMENT ON TABLE analyticalsampling.mixturemethod
	IS 'Method used to mix the samples (e.g bulk, pool)'
;

COMMENT ON TABLE analyticalsampling.sampledetail
	IS 'Specimen of the crop taken for observation, testing, or analysis'
;

COMMENT ON TABLE analyticalsampling.samplemixture
	IS 'Relation of samples combined in to one mixture'
;

COMMENT ON TABLE analyticalsampling.samplemixturedetail
	IS 'Details of the '
;

COMMENT ON TABLE analyticalsampling.shipment
	IS 'Details of the shipment.'
;

COMMENT ON TABLE analyticalsampling.shipmentset
	IS 'Set of containers to be shipped.'
;

COMMENT ON TABLE analyticalsampling.storagelocation
	IS 'Physical location where a seed is stored'
;

COMMENT ON TABLE analyticalsampling.study
	IS 'A set of samples to be analyzed'
;

COMMENT ON TABLE experiment.location
	IS 'Location: Planting area where occurrences of experiments are mapped and planted [LOC]'
;

COMMENT ON TABLE experiment.plant
	IS 'Plant: Plant that is planted in a plot or maybe a subplot [PLANT]'
;

COMMENT ON TABLE experiment.plot
	IS 'Plot: Particular place where the entry is planted and if the entry is planted more than once, it is called the replication of the entry [PLOT]'
;

COMMENT ON TABLE experiment.subplot
	IS 'Subplot: Subordinate of a plot, which may be a hill, a plant or another subplot [SUBPLOT]'
;

COMMENT ON TABLE germplasm."cross"
	IS 'Cross: Combination of parental germplasm to create a cross [CROSS]'
;

COMMENT ON TABLE germplasm.cross_attribute
	IS 'Cross Attribute: Attribute linked to the cross [CROSSATTR]'
;

COMMENT ON TABLE germplasm.cross_parent
	IS 'Cross Parent: Parent germplasm that, when crossed with another parent, can produce a cross/new germplasm [CPARENT]'
;

COMMENT ON TABLE germplasm.envelope
	IS 'Envelope: Smaller packets of seeds taken from a source seed package [ENVLP]'
;

COMMENT ON TABLE germplasm.family
	IS 'Family: Family consists of the genealogy of germplasm [FAM]'
;

COMMENT ON TABLE germplasm.genetic_population
	IS 'Examination and modeling of genetic variation within populations of germplasm\n\nQUESTION/COMMENT: Requesting for sample data for this entity'
;

COMMENT ON TABLE germplasm.genetic_population_attribute
	IS 'Attribute linked to the genetic population'
;

COMMENT ON TABLE germplasm.germplasm
	IS 'Germplasm: Genetic material of a crop developed and produced in experiments in various breeding programs [GERM]'
;

COMMENT ON TABLE germplasm.germplasm_attribute
	IS 'Germplasm Attribute: Attribute linked to the germplasm [GERMATTR]'
;

COMMENT ON TABLE germplasm.germplasm_name
	IS 'Germplasm Name: Name designated to the germplasm [GERMNAME]'
;

COMMENT ON TABLE germplasm.germplasm_relation
	IS 'Germplasm Relation: Relationship of a germplasm with another germplasm [GERMREL]'
;

COMMENT ON TABLE germplasm.germplasm_state_attribute
	IS 'Germplasm State Attribute: Rationale of the state of the germplasm [GSATTR]'
;

COMMENT ON TABLE germplasm.germplasm_trait
	IS 'Germplasm Trait: Trait observed from the germplasm [GERMTRAIT]'
;

COMMENT ON TABLE germplasm.package
	IS 'Package: Physical bag of seeds [PKG]'
;

COMMENT ON TABLE germplasm.package_relation
	IS 'Package Relation: Association of the package with another package [PKGREL]'
;

COMMENT ON TABLE germplasm.package_trait
	IS 'Package Trait: Trait related to the package [PKGTRAIT]'
;

COMMENT ON TABLE germplasm.product_profile
	IS 'Product Profile: Outcome of the development and testing of germplasm in a defined scheme, which satisfies the requirements prepared by a program like possessing desirable traits to target a particular market segment [PRODPROF]'
;

COMMENT ON TABLE germplasm.product_profile_attribute
	IS 'Product Profile Data: Additional information of the product profile [PPATTR]'
;

COMMENT ON TABLE germplasm.seed
	IS 'Seed: Genetic entity of a germplasm [SEED]'
;

COMMENT ON TABLE germplasm.seed_attribute
	IS 'Seed Attribute: Attribute linked to the seed [SEEDATTR]'
;

COMMENT ON TABLE germplasm.seed_relation
	IS 'Seed Relation: Relation of a seed with another seed, possible due to the descent of a seed from another seed or a combination of two distinct seeds of the same germplasm [SEEDREL]'
;

COMMENT ON TABLE germplasm.seed_trait
	IS 'Seed Trait: Trait linked to the seed [SEEDTRAIT]'
;

COMMENT ON TABLE germplasm.taxonomy
	IS 'Taxonomy: Schema of classifying a germplasm [TAXON]'
;

COMMENT ON TABLE platform.list
	IS 'Table for storing custom lists for users that they can use in different applications in the system'
;

COMMENT ON TABLE platform.listmember
	IS 'Stores the items that are added to a specific list.'
;

COMMENT ON COLUMN analyticalsampling.shipment.comment
	IS 'We have to register the date register, date send and  date receive'
;

COMMENT ON COLUMN experiment.location.description
	IS 'Location Description: Additional information about the location [LOC_DESC]'
;

COMMENT ON COLUMN experiment.location.location_code
	IS 'Location Code: Textual identifier of the location [LOC_CODE]'
;

COMMENT ON COLUMN experiment.location.location_document
	IS 'Sorted list of distinct lexemes which are normalized; used in search query'
;

COMMENT ON COLUMN experiment.location.location_harvest_date
	IS 'Location Harvest Date: Date in general when the plots in the location were harvested [LOC_HARVDATE]'
;

COMMENT ON COLUMN experiment.location.location_name
	IS 'Location Name: Full name of the location [LOC_NAME]'
;

COMMENT ON COLUMN experiment.location.location_planting_date
	IS 'Location Planting Date: Date in general when the entries in the location were planted [LOC_PLANTDATE]'
;

COMMENT ON COLUMN experiment.location.location_status
	IS 'Location Status: Status of the location {draft, operational, completed} [LOC_STATUS]'
;

COMMENT ON COLUMN experiment.location.location_type
	IS 'Location Type: Type of the location, which can be fixed or movable {greenhouse, base (zone in a greenhouse with pots/plants), etc.} [LOC_TYPE]'
;

COMMENT ON COLUMN experiment.location.location_year
	IS 'Location Year: Experiment location year'
;

COMMENT ON COLUMN experiment.location.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN experiment.plant.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN experiment.plant.plant_code
	IS 'Plant Code: Textual identifier of the plant [PLANT_CODE]'
;

COMMENT ON COLUMN experiment.plot.design_x
	IS 'Design X: Coordinate paired with Design Y that is generated by randomization and is used for data analysis [PLOT_DESX]'
;

COMMENT ON COLUMN experiment.plot.design_y
	IS 'Design Y: Coordinate paired with Design X that is generated by randomization and is used for data analysis [PLOT_DESY]'
;

COMMENT ON COLUMN experiment.plot.field_x
	IS 'Field X: Coordinate paired with Field Y that determines the placement of the plot in the field [PLOT_FLDX]'
;

COMMENT ON COLUMN experiment.plot.field_y
	IS 'Field Y: Coordinate paired with Field X that determines the placement of the plot in the field [PLOT_FLDY]'
;

COMMENT ON COLUMN experiment.plot.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN experiment.plot.pa_x
	IS 'Planting Area X: Coordinate paired with Planting Area Y that is generated by KDX or other tools and is used for mapping the plots in the planting area [PLOT_PAX]'
;

COMMENT ON COLUMN experiment.plot.pa_y
	IS 'Planting Area Y: Coordinate paired with Planting Area X that is generated by KDX or other tools and is used for mapping the plots in the planting area [PLOT_PAY]'
;

COMMENT ON COLUMN experiment.plot.plot_code
	IS 'Plot Code: Textual identifier of the plot within the location, which is used in data analysis [PLOT_CODE]'
;

COMMENT ON COLUMN experiment.plot.plot_number
	IS 'Plot Number: Ordering of the plot within the location, which is used in data analysis [PLOT_NO]'
;

COMMENT ON COLUMN experiment.plot.plot_order_number
	IS 'Plot Order Number: Order number of plot in the planting area [PLOT_ORDERNO]'
;

COMMENT ON COLUMN experiment.plot.plot_qc_code
	IS 'Plot QC Code: Quality control code assigned to the plot {G (good), Q (questionable), S (suppressed)} [PLOT_QCCODE]'
;

COMMENT ON COLUMN experiment.plot.plot_status
	IS 'Plot Status: Status of the plot {draft, active, replaced} [PLOT_STATUS]'
;

COMMENT ON COLUMN experiment.plot.plot_type
	IS 'Plot Type: Type of the plot {options depend on the organization} [PLOT_TYPE]'
;

COMMENT ON COLUMN experiment.plot.rep
	IS 'Plot Replication: Replication number of the entry in the plot [PLOT_REP]'
;

COMMENT ON COLUMN experiment.subplot.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN experiment.subplot.subplot_code
	IS 'Subplot Code: Textual identifier of the subplot [SUBPLOT_CODE]'
;

COMMENT ON COLUMN experiment.subplot.subplot_number
	IS 'Subplot Number: Sequential number of the subplot within the plot [SUBPLOT_NO]'
;

COMMENT ON COLUMN experiment.subplot.subplot_type
	IS 'Subplot Type: Type of the subplot {hill, row, any part of plant, ...} [SUBPLOT_TYPE]'
;

COMMENT ON COLUMN experiment.subplot.subplot_z_number
	IS 'Subplot Z Number: Ordering of subplot in the plot or another subplot [SUBPLOT_ZNO]'
;

COMMENT ON COLUMN germplasm."cross".cross_method
	IS 'Cross Method: Method of crossing used in the cross {single cross, backcross, ...} [CROSS_METHOD]'
;

COMMENT ON COLUMN germplasm."cross".cross_name
	IS 'Cross Name: Name of the cross, temporarily set to the combination of the parents'' names (A/B) but when marked as successfull, it is assigned with a cross name that complies with the naming convention of the organization [CROSS_NAME]'
;

COMMENT ON COLUMN germplasm."cross".notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.cross_attribute.data_qc_code
	IS 'Cross Attribute Data QC Code: Quality of the cross attribute {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [CROSSATTR_QCCODE]'
;

COMMENT ON COLUMN germplasm.cross_attribute.data_value
	IS 'Cross Attribute Data Value: Value of the cross attribute [CROSSATTR_DATAVAL]'
;

COMMENT ON COLUMN germplasm.cross_attribute.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.cross_parent.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.cross_parent.order_number
	IS 'Order Number: Ordering of parents in the cross [CPARENT_ORDERNO]'
;

COMMENT ON COLUMN germplasm.cross_parent.parent_role
	IS 'Parent Role: Role of the parent in the cross {female, male} [CPARENT_PARENTROLE]'
;

COMMENT ON COLUMN germplasm.envelope.envelope_code
	IS 'Envelope Code: Textual identifier of the envelope [ENVLP_CODE]'
;

COMMENT ON COLUMN germplasm.envelope.envelope_label
	IS 'Envelope Label: Human-readable text printed on the envelope [ENVLP_LABEL]'
;

COMMENT ON COLUMN germplasm.envelope.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.envelope.order_number
	IS 'Envelope Order Number: Ordering of the envelope within the package [ENVLP_ORDERNO]'
;

COMMENT ON COLUMN germplasm.family.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.family.selection_number
	IS 'Selection Number: Sequence order of the germplasm from the family, if applicable [FAM_SELNO]'
;

COMMENT ON COLUMN germplasm.genetic_population.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.genetic_population_attribute.data_qc_code
	IS 'Status of the genetic population attribute  {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)}'
;

COMMENT ON COLUMN germplasm.genetic_population_attribute.data_value
	IS 'Value of the genetic population attribute'
;

COMMENT ON COLUMN germplasm.genetic_population_attribute.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.genetic_population_attribute.variable_id
	IS 'Reference to the variable of the genetic population attribute'
;

COMMENT ON COLUMN germplasm.germplasm.designation
	IS 'Designation: Standard designation/name of the germplasm [GERM_DESIGNATION]'
;

COMMENT ON COLUMN germplasm.germplasm.generation
	IS 'Generation: Stage of development of a germplasm [GERM_GENERATION]'
;

COMMENT ON COLUMN germplasm.germplasm.germplasm_name_type
	IS 'Germplasm Name Type: Type of name designated to the germplasm [GERM_NAMETYPE]'
;

COMMENT ON COLUMN germplasm.germplasm.germplasm_normalized_name
	IS 'Germplasm Normalized Name: Formatted germplasm name that is standardized for query purposes [GERM_NORMNAME]'
;

COMMENT ON COLUMN germplasm.germplasm.germplasm_state
	IS 'Germplasm State: Current state of the germplasm: (progeny, fixed line, ...) [GERM_STATE]'
;

COMMENT ON COLUMN germplasm.germplasm.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.germplasm.parentage
	IS 'Parentage: Parentage string of the germplasm [GERM_PARENTAGE]'
;

COMMENT ON COLUMN germplasm.germplasm_attribute.data_qc_code
	IS 'Germlasm Attribute Data QC Code: Status of the germplasm attribute {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [GERMATTR_QCCODE]'
;

COMMENT ON COLUMN germplasm.germplasm_attribute.data_value
	IS 'Germplasm Attribute Data Value: Value of the germplasm attribute [GERMATTR_DATAVAL]'
;

COMMENT ON COLUMN germplasm.germplasm_attribute.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.germplasm_name.germplasm_name_status
	IS 'Germplasm Name Status: Status of the germplasm name {standard, active, deprecated, inactive, voided} [GERMNAME_NAMESTATUS]'
;

COMMENT ON COLUMN germplasm.germplasm_name.germplasm_name_type
	IS 'Germplasm Name Type: Type of name designated to the germplasm [GERMNAME_NAMETYPE]'
;

COMMENT ON COLUMN germplasm.germplasm_name.germplasm_normalized_name
	IS 'Germplasm Normalized Name: Formatted germplasm name that is standardized for query purposes [GERMNAME_NORMNAME]'
;

COMMENT ON COLUMN germplasm.germplasm_name.name_value
	IS 'Germplasm Name Value: Value of the name designated to the germplasm [GERMNAME_NAMEVAL]'
;

COMMENT ON COLUMN germplasm.germplasm_name.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.germplasm_relation.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.germplasm_relation.order_number
	IS 'Germplasm Relation Order Number: Ordering of the child germplasm within the parent germplasm [GERMREL_ORDERNO]'
;

COMMENT ON COLUMN germplasm.germplasm_state_attribute.germplasm_state_rationale
	IS 'Germplasm State Rationale: Reason of the state of the germplasm [GSATTR_RATIONALE]'
;

COMMENT ON COLUMN germplasm.germplasm_state_attribute.germplasm_state_type
	IS 'Germplasm State Type: Type of state of the germplasm [GSATTR_STATETYPE]'
;

COMMENT ON COLUMN germplasm.germplasm_state_attribute.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.germplasm_trait.data_qc_code
	IS 'Germplasm Trait Data QC Code: Quality of the germplasm trait {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [GERMTRAIT_QCCODE]'
;

COMMENT ON COLUMN germplasm.germplasm_trait.data_value
	IS 'Germplasm Trait Data Value: Value of the germplasm trait [GERMTRAIT_DATAVAL]'
;

COMMENT ON COLUMN germplasm.germplasm_trait.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.package.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.package.package_code
	IS 'Package Code: Textual identifier of the package [PKG]'
;

COMMENT ON COLUMN germplasm.package.package_label
	IS 'Package Label: Human-readable text printed on the seed package [PKG_LABEL]'
;

COMMENT ON COLUMN germplasm.package.package_quantity
	IS 'Package Quantity: Weight or number of seeds in the package [PKG_QTY]'
;

COMMENT ON COLUMN germplasm.package.package_status
	IS 'Package Status: Status of the package {list values here} [PKG_STATUS]'
;

COMMENT ON COLUMN germplasm.package.package_unit
	IS 'Package Unit: Unit of quantity of the seed package [PKG_UNIT]'
;

COMMENT ON COLUMN germplasm.package_relation.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.package_relation.relation_type
	IS 'Relation Type: Type of relation {combined (one or more source packages to produce one destination package), divided (one source package to produce one or many destination packages), subset (one source package to product one or many destination packages without the need to generate new seeds)} [PKGREL_RELTYPE]'
;

COMMENT ON COLUMN germplasm.package_trait.data_qc_code
	IS 'Package Trait Data QC Code: Quality of the package trait {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [PKGTRAIT_QCCODE]'
;

COMMENT ON COLUMN germplasm.package_trait.data_value
	IS 'Package Trait Data Value: Value of the package trait [PKGTRAIT_DATAVAL]'
;

COMMENT ON COLUMN germplasm.package_trait.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.product_profile.description
	IS 'Product Profile Description: Additional information about the product profile [PRODPROF_DESC]'
;

COMMENT ON COLUMN germplasm.product_profile.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.product_profile.product_profile_code
	IS 'Product Profile Code: Textual identifier of the product profile [PRODPROF_CODE]'
;

COMMENT ON COLUMN germplasm.product_profile.product_profile_name
	IS 'Product Profile Name: Full name of the product profile [PRODPROF_NAME]'
;

COMMENT ON COLUMN germplasm.product_profile_attribute.data_value
	IS 'Product Profile Attribute Value: Value of the product profile attribute [PPATTR_DATAVAL]'
;

COMMENT ON COLUMN germplasm.product_profile_attribute.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.product_profile_attribute.variable_id
	IS 'Variable ID: Referene to the variable of the product profile data [PPATTR_VAR_ID]'
;

COMMENT ON COLUMN germplasm.seed.description
	IS 'Seed Description: Additional information about the seed [SEED_DESC]'
;

COMMENT ON COLUMN germplasm.seed.harvest_date
	IS 'Seed Harvest Date: Date when the seeds were harvested from the location of an experiment [SEED_HARVESTDATE]'
;

COMMENT ON COLUMN germplasm.seed.harvest_method
	IS 'Harvest Method: Method of harvesting the plots in the location [SEED_HARVMETH]'
;

COMMENT ON COLUMN germplasm.seed.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.seed.seed_code
	IS 'Seed Code: Textual identifier of the seed [SEED_CODE]'
;

COMMENT ON COLUMN germplasm.seed.seed_name
	IS 'Seed Name: Full name of the germplasm at the time the seed was harvested [SEED_NAME]'
;

COMMENT ON COLUMN germplasm.seed_attribute.data_qc_code
	IS 'Seed Attribute Data QC Code: Quality of the seed attribute {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [SEEDATTR_QCCODE]'
;

COMMENT ON COLUMN germplasm.seed_attribute.data_value
	IS 'Seed Attribute Data Value: Value of the seed attribute [SEEDATTR_DATAVAL]'
;

COMMENT ON COLUMN germplasm.seed_attribute.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.seed_relation.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.seed_relation.order_number
	IS 'Seed Relation Order Number: Order of the child seed within the parent seed [SEEDREL_ORDERNO]'
;

COMMENT ON COLUMN germplasm.seed_trait.data_qc_code
	IS 'Seed Trait Data QC Code: Status of the seed trait {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [SEEDTRAIT_QCCODE]'
;

COMMENT ON COLUMN germplasm.seed_trait.data_value
	IS 'Seed Trait Data Value: Value of the seed trait [SEEDTRAIT_DATAVAL]'
;

COMMENT ON COLUMN germplasm.seed_trait.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.taxonomy.description
	IS 'Taxonomy Description: Additional information about the taxonomy [TAXON_DESC]'
;

COMMENT ON COLUMN germplasm.taxonomy.notes
	IS 'NOTES: Technical details about the record [ISVOID]'
;

COMMENT ON COLUMN germplasm.taxonomy.taxon_id
	IS 'Taxon ID: Recognized identity or essential character of the germplasm [TAXON_ID]'
;

COMMENT ON COLUMN germplasm.taxonomy.taxonomy_name
	IS 'Taxonomy Name: Full name of the taxonomic identity [TAXON_NAME]'
;

COMMENT ON COLUMN platform.list.abbrev
	IS 'Abbrev of the list. must be unique'
;

COMMENT ON COLUMN platform.list.description
	IS 'User description of the list'
;

COMMENT ON COLUMN platform.list.display_name
	IS 'Display name of the list'
;

COMMENT ON COLUMN platform.list.event_log
	IS 'Historical transactions of the record'
;

COMMENT ON COLUMN platform.list.name
	IS 'Name of the list'
;

COMMENT ON COLUMN platform.list.notes
	IS 'Additional details added by an admin; can be technical or advanced details'
;

COMMENT ON COLUMN platform.list.record_uuid
	IS 'Universally unique identifier (UUID) of the record'
;

COMMENT ON COLUMN platform.list.remarks
	IS 'Additional details about the record'
;

COMMENT ON COLUMN platform.list.type
	IS 'List type can be designation, plot, seed, study, or location'
;

COMMENT ON COLUMN platform.listmember.data_id
	IS 'Data ID of the entity. If list type is study, data_id will refer to operational.study.id'
;

COMMENT ON COLUMN platform.listmember.display_value
	IS 'Display value of the item member in list. Can be designation, study_name, or plot code'
;

COMMENT ON COLUMN platform.listmember.event_log
	IS 'Historical transactions of the record'
;

COMMENT ON COLUMN platform.listmember.notes
	IS 'Additional details added by an admin; can be technical or advanced details'
;

COMMENT ON COLUMN platform.listmember.order_number
	IS 'Order number of the member in the list'
;

COMMENT ON COLUMN platform.listmember.record_uuid
	IS 'Universally unique identifier (UUID) of the record'
;

COMMENT ON COLUMN platform.listmember.remarks
	IS 'Additional details about the record'
;

COMMENT ON COLUMN tenant.entityreference.entity
	IS ''
;

COMMENT ON COLUMN tenant.entityreference.storefield
	IS ''
;

COMMENT ON COLUMN tenant.entityreference.textfield
	IS ''
;

COMMENT ON COLUMN tenant.entityreference.valuefield
	IS ''
;

COMMIT;
