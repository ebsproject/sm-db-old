-- Deploy analyticalsampling:change_schema_vendorcontrol to pg

BEGIN;

ALTER TABLE analyticalsampling.sampledetail 
  DROP  CONSTRAINT "FK_sampledetail_vendorcontrol";

DROP TABLE IF EXISTS public.vendorcontrol_batch;

DROP TABLE IF EXISTS public.vendorcontrol;

DROP SEQUENCE IF EXISTS public.vendorcontrol_id_seq;

CREATE TABLE analyticalsampling.vendorcontrol
(
	position varchar(50) NULL,	-- Position of the technology platform control. e.g. H11, H12, etc.
	tenant_id integer NOT NULL,
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NOT NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('analyticalsampling."vendorcontrol_id_seq"'::text)::regclass),
	technologyplatform_id integer NULL
)
;

CREATE TABLE analyticalsampling.vendorcontrol_batch
(
	batch_id integer NULL,
	vendorcontrol_id integer NULL
)
;

CREATE SEQUENCE analyticalsampling.vendorcontrol_id_seq INCREMENT 1 START 1;


ALTER TABLE analyticalsampling.vendorcontrol ADD CONSTRAINT "PK_vendorcontrol"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.sampledetail ADD CONSTRAINT "FK_sampledetail_vendorcontrol"
	FOREIGN KEY (vendorcontrol_id) REFERENCES analyticalsampling.vendorcontrol (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.vendorcontrol ADD CONSTRAINT "FK_vendorcontrol_technologyplatform"
	FOREIGN KEY (technologyplatform_id) REFERENCES analyticalsampling.technologyplatform (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.vendorcontrol_batch ADD CONSTRAINT "FK_vendorcontrol_batch_batch"
	FOREIGN KEY (batch_id) REFERENCES analyticalsampling.batch (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.vendorcontrol_batch ADD CONSTRAINT "FK_vendorcontrol_batch_vendorcontrol"
	FOREIGN KEY (vendorcontrol_id) REFERENCES analyticalsampling.vendorcontrol (id) ON DELETE No Action ON UPDATE No Action
;


COMMIT;
