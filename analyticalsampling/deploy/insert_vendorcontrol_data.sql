-- Deploy analyticalsampling:insert_vendorcontrol_data to pg

BEGIN;


INSERT INTO analyticalsampling.dataformat
(id, "name", description, tenant_id, creation_timestamp, creator_id, is_void)
values
(1, 'csv', 'CSV', 1, now(), 'System', false),
(2, 'hdf5', 'HDF5', 1, now(), 'System', false),
(3, 'xls', 'Excel file', 1, now(), 'System', false)
;

INSERT INTO tenant.vendor
(id, code, reference, status, tenant_id, creation_timestamp, creator_id, is_void, person_id, dataformat_id)
values
(1, 'Intertek', 'INtertek', 'Active', 1, now(), 'System', false, 34, 1),
(2, 'DArT', 'DArT', 'Active', 1, now(), 'System', false, 34, 1)
;

INSERT INTO analyticalsampling.technologyplatform
(id, "name", description, tenant_id, creation_timestamp, creator_id, is_void, vendor_id )
values
(1, 'low density', 'low density', 1, now(), 'System', false, 1),
(2, 'mid density', 'mid density', 1, now(), 'System', false, 1),
(3, 'low density', 'low density', 1, now(), 'System', false, 2),
(4, 'mid density', 'mid density', 1, now(), 'System', false, 2),
(5, 'high density', 'mid density', 1, now(), 'System', false, 2)
;


INSERT INTO analyticalsampling.vendorcontrol
(id, "position", tenant_id, creation_timestamp, creator_id, is_void, technologyplatform_id)
values
(1, 'H11,H12', 1, now(), 'System', false, 1),
(2, 'G12,H12', 1, now(), 'System', false, 2),
(3, 'G12,H12', 1, now(), 'System', false, 4),
(4, 'User defined', 1, now(), 'System', false, 1)
;

SELECT setval('analyticalsampling.dataformat_id_seq', (SELECT MAX(id) FROM analyticalsampling.dataformat));
SELECT setval('tenant.vendor_id_seq', (SELECT MAX(id) FROM tenant.vendor));
SELECT setval('analyticalsampling.technologyplatform_id_seq', (SELECT MAX(id) FROM analyticalsampling.technologyplatform));
SELECT setval('analyticalsampling.vendorcontrol_id_seq', (SELECT MAX(id) FROM analyticalsampling.vendorcontrol));

COMMIT;
