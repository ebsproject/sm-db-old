-- Deploy analyticalsampling:update_requestlistmember_batch to pg

BEGIN;

DROP TABLE IF EXISTS analyticalsampling.requestlistmember_batch;

ALTER TABLE analyticalsampling.batch 
 ALTER COLUMN creation_timestamp SET NOT NULL
;
ALTER TABLE analyticalsampling.batch 
 ALTER COLUMN creator_id SET NOT NULL
;
ALTER TABLE analyticalsampling.batch 
 ALTER COLUMN tenant_id SET NOT NULL
;
ALTER TABLE analyticalsampling.batchcode 
 ALTER COLUMN creation_timestamp SET NOT NULL
;
ALTER TABLE analyticalsampling.batchcode 
 ALTER COLUMN creator_id SET NOT NULL
;
ALTER TABLE analyticalsampling.batchcode 
 ALTER COLUMN tenant_id SET NOT NULL
;
ALTER TABLE analyticalsampling.requestlistmember 
 ALTER COLUMN creation_timestamp SET NOT NULL
;
ALTER TABLE analyticalsampling.requestlistmember 
 ALTER COLUMN creator_id SET NOT NULL
;
ALTER TABLE analyticalsampling.requestlistmember 
 ALTER COLUMN tenant_id SET NOT NULL
;
ALTER TABLE tenant.vendor 
 ALTER COLUMN creation_timestamp SET NOT NULL
;
ALTER TABLE tenant.vendor 
 ALTER COLUMN creator_id SET NOT NULL
;
ALTER TABLE tenant.vendor 
 ALTER COLUMN tenant_id SET NOT NULL
;
ALTER TABLE analyticalsampling.batch 
 ALTER COLUMN creation_timestamp SET DEFAULT now()
;
ALTER TABLE analyticalsampling.batchcode 
 ALTER COLUMN creation_timestamp SET DEFAULT now()
;
ALTER TABLE analyticalsampling.requestlistmember 
 ALTER COLUMN creation_timestamp SET DEFAULT now()
;
ALTER TABLE tenant.vendor 
 ALTER COLUMN creation_timestamp SET DEFAULT now()
;
ALTER TABLE analyticalsampling.batch 
 ADD COLUMN in_design boolean NULL
;
ALTER TABLE analyticalsampling.batch 
 ADD COLUMN serviceprovider_id integer NULL
;
ALTER TABLE analyticalsampling.batchcode 
 ADD COLUMN last_plate_number integer NULL
;
ALTER TABLE analyticalsampling.requestlistmember 
 ADD COLUMN batch_id integer NULL
;
ALTER TABLE analyticalsampling.batch 
 ADD COLUMN vendor_id integer NULL
;

ALTER TABLE analyticalsampling.batch ADD CONSTRAINT "FK_batch_vendor"
	FOREIGN KEY (vendor_id) REFERENCES tenant.vendor (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.batch ADD CONSTRAINT "FK_batch_serviceprovider"
	FOREIGN KEY (serviceprovider_id) REFERENCES analyticalsampling.serviceprovider (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE analyticalsampling.requestlistmember ADD CONSTRAINT "FK_requestlistmember_batch"
	FOREIGN KEY (batch_id) REFERENCES analyticalsampling.batch (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON COLUMN analyticalsampling.purpose.description
	IS 'description of the purpose'
;

COMMENT ON COLUMN analyticalsampling.service.description
	IS 'Description of the Service'
;

COMMENT ON COLUMN analyticalsampling.serviceprovider.name
	IS 'Name of the Service Provider'
;

COMMENT ON COLUMN analyticalsampling.purpose.description
	IS 'description of the purpose'
;

COMMENT ON COLUMN analyticalsampling.service.description
	IS 'Description of the Service'
;

COMMENT ON COLUMN analyticalsampling.serviceprovider.name
	IS 'Name of the Service Provider'
;

COMMIT;
