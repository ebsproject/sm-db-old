-- Deploy analyticalsampling:remove_b4r_tables to pg

BEGIN;

ALTER TABLE analyticalsampling.requestlistmember 
  DROP  CONSTRAINT "FK_requestlistmember_listmember";

ALTER TABLE analyticalsampling.sample 
  DROP  CONSTRAINT "FK_sample_entity";

ALTER TABLE analyticalsampling.sampledetail 
  DROP  CONSTRAINT "FK_sampledetail_entity";

ALTER TABLE analyticalsampling.sampledetail 
  DROP  CONSTRAINT "FK_sampledetail_listmember";

ALTER TABLE analyticalsampling.samplemixturedetail 
  DROP  CONSTRAINT "FK_samplemixturedetail_listmember";

DROP TABLE IF EXISTS experiment.plant
;
DROP TABLE IF EXISTS germplasm.family
;
DROP TABLE IF EXISTS germplasm.cross_attribute
;
DROP TABLE IF EXISTS germplasm.cross_parent
;
DROP TABLE IF EXISTS germplasm."cross"
;
DROP TABLE IF EXISTS germplasm.envelope
;
DROP TABLE IF EXISTS germplasm.genetic_population_attribute
;
DROP TABLE IF EXISTS germplasm.genetic_population
;
DROP TABLE IF EXISTS germplasm.germplasm_attribute
;
DROP TABLE IF EXISTS germplasm.germplasm_name
;
DROP TABLE IF EXISTS germplasm.germplasm_relation
;
DROP TABLE IF EXISTS germplasm.germplasm_state_attribute
;
DROP TABLE IF EXISTS germplasm.germplasm_trait
;
DROP TABLE IF EXISTS germplasm.package_relation
;
DROP TABLE IF EXISTS germplasm.package_trait
;
DROP TABLE IF EXISTS germplasm.package
;
DROP TABLE IF EXISTS germplasm.product_profile_attribute
;
DROP TABLE IF EXISTS germplasm.seed_attribute
;
DROP TABLE IF EXISTS germplasm.seed_relation
;
DROP TABLE IF EXISTS germplasm.seed_trait
;
DROP TABLE IF EXISTS germplasm.seed
;
DROP TABLE IF EXISTS germplasm.germplasm
;
DROP TABLE IF EXISTS germplasm.product_profile
;
DROP TABLE IF EXISTS germplasm.taxonomy
;
DROP TABLE IF EXISTS platform.listmember
;
DROP TABLE IF EXISTS platform.list
;
DROP TABLE IF EXISTS dictionary.entity
;
DROP TABLE IF EXISTS experiment.subplot
;
DROP TABLE IF EXISTS experiment.plot
;

ALTER TABLE analyticalsampling.requestlistmember 
 ADD COLUMN data_id integer NULL
;
ALTER TABLE analyticalsampling.requestlistmember 
 ADD COLUMN entity_id integer NULL
;
ALTER TABLE analyticalsampling.samplemixturedetail 
 ADD COLUMN data_id integer NULL
;
ALTER TABLE experiment.location 
 ADD COLUMN location_number integer NULL	-- Location Number: Sequential number of the location within the same year and season [LOC_NO]
;

COMMENT ON COLUMN experiment.location.location_number
	IS 'Location Number: Sequential number of the location within the same year and season [LOC_NO]'
;

COMMIT;
