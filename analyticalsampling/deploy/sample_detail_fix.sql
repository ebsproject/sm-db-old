-- Deploy analyticalsampling:sample_detail_fix to pg

BEGIN;

ALTER TABLE analyticalsampling.sampledetail 
  DROP  CONSTRAINT "FK_sampledetail_containerset";

ALTER TABLE analyticalsampling.sampledetail 
  DROP  CONSTRAINT "FK_sampledetail_session";

ALTER TABLE analyticalsampling.requestlistmember 
 DROP COLUMN IF EXISTS listmember_id;

ALTER TABLE analyticalsampling.sampledetail 
 DROP COLUMN IF EXISTS listmember_id;

ALTER TABLE analyticalsampling.sampledetail 
 DROP COLUMN IF EXISTS session_id;

ALTER TABLE analyticalsampling.sampledetail 
 ALTER COLUMN containerset_id DROP NOT NULL;

ALTER TABLE analyticalsampling.requestlistmember 
 ADD COLUMN list_id integer NULL;

ALTER TABLE analyticalsampling.sampledetail 
 ADD COLUMN list_id integer NULL;

ALTER TABLE analyticalsampling.sampledetail 
 ADD COLUMN vendorcontrol_id integer NULL;

 ALTER TABLE analyticalsampling.sampledetail 
 ALTER COLUMN uuid SET DEFAULT public.uuid_generate_v4();

CREATE TABLE vendorcontrol
(
	position varchar(50) NULL,	-- Position of the technology platform control. e.g. H11, H12, etc.
	tenant_id integer NOT NULL,
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),
	modification_timestamp timestamp without time zone NULL,
	creator_id varchar(50) NOT NULL,
	modifier_id varchar(50) NULL,
	is_void boolean NOT NULL   DEFAULT false,
	id integer NOT NULL   DEFAULT NEXTVAL(('"vendorcontrol_id_seq"'::text)::regclass),
	technologyplatform_id integer NULL
)
;

CREATE TABLE vendorcontrol_batch
(
	batch_id integer NULL,
	vendorcontrol_id integer NULL
)
;

CREATE SEQUENCE vendorcontrol_id_seq INCREMENT 1 START 1;

ALTER TABLE analyticalsampling.sampledetail ADD CONSTRAINT "FK_sampledetail_containerset"
	FOREIGN KEY (containerset_id) REFERENCES analyticalsampling.containerset (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE vendorcontrol ADD CONSTRAINT "PK_vendorcontrol"
	PRIMARY KEY (id)
;

ALTER TABLE analyticalsampling.sampledetail ADD CONSTRAINT "FK_sampledetail_vendorcontrol"
	FOREIGN KEY (vendorcontrol_id) REFERENCES vendorcontrol (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE vendorcontrol ADD CONSTRAINT "FK_vendorcontrol_technologyplatform"
	FOREIGN KEY (technologyplatform_id) REFERENCES analyticalsampling.technologyplatform (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE vendorcontrol_batch ADD CONSTRAINT "FK_vendorcontrol_batch_batch"
	FOREIGN KEY (batch_id) REFERENCES analyticalsampling.batch (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE vendorcontrol_batch ADD CONSTRAINT "FK_vendorcontrol_batch_vendorcontrol"
	FOREIGN KEY (vendorcontrol_id) REFERENCES vendorcontrol (id) ON DELETE No Action ON UPDATE No Action
;

COMMIT;
