-- Deploy as_usecase:add_batch_and_locations_data to pg

BEGIN;

INSERT INTO experiment."location"
(id, location_code, location_name, location_status, location_type, description, location_planting_date, location_harvest_date, location_year, tenant_id, creation_timestamp, creator_id, is_void)
VALUES
(1, 'MIX', 'Mixture', 'Active', 'Mixture', 'Mixture', '2020', '2020', 2020, 1, now(), 'System', false),
(2, 'HQCIM', 'HQ CIMMYT', 'Active', 'HQ CIMMYT', 'HQ CIMMYT', '2020', '2020', 2020, 1, now(), 'System', false);

INSERT INTO analyticalsampling.collectioncontainertype
(id, "name", description, code, tenant_id, creation_timestamp, creator_id, is_void)
VALUES(1, 'plate 96', 'plate', 'P', 1, now(), 'System', false);

INSERT INTO analyticalsampling.collectionlayout
(id, num_columns, num_rows, num_well, tenant_id, creation_timestamp, creator_id, is_void, collectioncontainertype_id)
VALUES(1, 10, 50, 500, 1, now(), 'System', false, 1);

INSERT INTO analyticalsampling.batchcode
(id, "name", code, last_sample_number, tenant_id, creation_timestamp, creator_id, is_void, serviceprovider_id, last_batch_number)
VALUES(1, 'BS1004BW-DW-QC200313', 'BS1004BW-DW-QC200313', 342, 1, now(), 'System', false, 4, 1);

UPDATE analyticalsampling.batchcode SET last_plate_number = 0;
UPDATE analyticalsampling.batchcode SET last_batch_number =0;
UPDATE analyticalsampling.batchcode SET last_sample_number =0;

SELECT setval('experiment.location_id_seq', (SELECT MAX(id) FROM experiment."location"));
SELECT setval('analyticalsampling.collectioncontainertype_id_seq', (SELECT MAX(id) FROM analyticalsampling.collectioncontainertype));
SELECT setval('analyticalsampling.collectionlayout_id_seq', (SELECT MAX(id) FROM analyticalsampling.collectionlayout));
SELECT setval('analyticalsampling.study_id_seq', (SELECT MAX(id) FROM analyticalsampling.batchcode));
SELECT setval('analyticalsampling.labproject_id_seq', (SELECT MAX(id) FROM analyticalsampling.batch));

COMMIT;
