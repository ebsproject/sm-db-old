-- Deploy as_usecase:load_catalogs_data to pg

BEGIN;

INSERT INTO analyticalsampling.loadtype
("name", description, tenant_id, creation_timestamp, creator_id, is_void)
values
('Columns', 'load by column', 1, now(), 'System', false),
('Rows', 'load by row', 1, now(), 'System', false),
('CIMMYT format', 'load by CIMMYT format', 1, now(), 'System', false)
;


INSERT INTO analyticalsampling.mixturemethod
("name", code, tenant_id, creation_timestamp, creator_id, is_void)
values
('Bulk', 'B', 1, NOW(), 'System', false),
('Pool', 'P', 1, NOW(), 'System', false)
;


INSERT INTO analyticalsampling.tissuetype
("name", description, tenant_id, creation_timestamp, creator_id, is_void)
values
('Seed', 'Seed', 1, now(), 'System', false),
('Leaf', 'Leaf', 1, now(), 'System', false)
;

COMMIT;
